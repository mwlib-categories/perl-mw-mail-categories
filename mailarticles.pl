
# MEDIAWIKI Category mailer
# COPYRIGHT (C) 2010 JAMES MICHAEL DUPONT <JamesMikeDuPont@googlemail.com>
# LICENSE AGPL 3.0
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.


use DBI();
use strict;
use warnings;
use MIME::Lite;

# Connect to the database.


# login to the database
sub Login
{
    my $wgDBtype           = "mysql";
    my $wgDBserver         = "localhost";
    my $wgDBname           = "wikidb2";
    my $wgDBuser           = "wikiuser";
    my $wgDBpassword       = "wikiuser";
 
    my $dbh = DBI->connect("DBI:$wgDBtype:database=$wgDBname;host=$wgDBserver",
			   $wgDBuser,  $wgDBpassword,
			   {'RaiseError' => 1});
    
    return $dbh;
}

sub getUsers
{
    my $dbh=shift;
    #
    my $users;
    my $sth = $dbh->prepare("select user_id,user_real_name,  user_email from user");
    $sth->execute();
    while (my $ref = $sth->fetchrow_hashref()) {
	print "Found a row: 
                          id = $ref->{'user_id'},
                          name = $ref->{'user_real_name'}
                          email = $ref->{'user_email'}\n";
	$users->{$ref->{'user_id'}}=$ref;
    }
    $sth->finish();
    return $users;
}


sub allWatchLists
{
    my $dbh=shift;
    my $watchlists=shift;
    my $sth = $dbh->prepare("
select page_title, cat_id, cat_title , cl_sortkey, wl_user, cl_from from categorylinks join category on category.cat_title= categorylinks.cl_to join watchlist on category.cat_title=watchlist.wl_title join page on cl_from=page_id

");
    $sth->execute();
    while (my $ref = $sth->fetchrow_hashref()) {
	$watchlists->{USERS}{$ref->{'wl_user'}}{$ref->{'page_title'}}=$ref;
	$watchlists->{PAGES}{$ref->{'page_title'}}=$ref;
    }
    $sth->finish();

    return $watchlists;
}

# now we extract the unique pages 
sub MakeAllUserPages
{
    my $users =shift;
    my $watchlists = shift;

    foreach my $user (keys %{$users})
    {		    
	my $ref = $users->{$user};
	my $uname = $ref->{'user_real_name'};
	my $email = $ref->{'user_email'};
	my $filename="user_${user}.json";
	my $filenamepdf="user_${user}.pdf";


	# create a json file to select those articles from the wiki
	open JSON, ">$filename" or die "Cannot open $filename";

	print JSON q!{ "items": [!;
	my $count =0;

# simple hack of commas... lazy
	print JSON join (",",
			 map {
			     q!{"content_type": "text/x-wiki","title": "! . 
				 $_ . 
				 q!","type": "article"}! 
			 }
			 (sort keys %{$watchlists->{USERS}{$user}})
	    );

	#TODO you will need to update this, see the metabook.json of your example zip files
	print JSON q!],     "licenses": [        {        "mw_license_url": "http://en.wikipedia.org/w/index.php?title=Help:Books/License&action=raw",             "type": "license"        }    ],     "summary": "",     "type": "collection",     "version": 1,     "wikis": [        {            "baseurl": "http://en.wikipedia.org/w/",             "imagesize": 800,             "keep_tmpfiles": false,             "output": "allarticles.zip",             "script_extension": ".php",             "type": "wikiconf"        }    ]}!;
	
	close JSON;
	

	# create the articles 
	system "mw-render -c allarticles.zip -m $filename -o $filenamepdf -w rl" ;


	# mail them
### Create a new multipart message:
	my $msg = MIME::Lite->new(
                 From    =>'jamesmikedupont@googlemail.com',
                 To      =>$email,
                 Subject =>'Your Subscription',
                 Type    =>'multipart/mixed'
                 );

    ### Add parts (each "attach" has same arguments as "new"):
	$msg->attach(Type     =>'TEXT',
                 Data     =>"Here's the PDF file you wanted"
	);
	$msg->attach(Type     =>'text/pdf',
                 Path     =>$filenamepdf,
                 Filename =>'wikiextract.pdf', # todo add a date flag
                 Disposition => 'attachment'
	);
	$msg->send;
    }# each user
}

sub GetAllNeededPages
{
    my $watchlists =shift;

    my $list =join " ", (sort keys %{$watchlists->{PAGES}});
#-m metabook.json
    my $command= "mw-zip -c :en  -o allarticles.zip " . $list;
    system $command;
    #foreach my $page (sort keys %{$watchlists->{PAGES}})
    #{print "getting $page\n";    }
    
}

## SIMPLE SIMPLE SIMPLE
my $dbh =Login(); # LOGIN
my $users =getUsers($dbh); # GET ALL USERS and emails
my $watchlists= allWatchLists($dbh); # get all watchlists

GetAllNeededPages $watchlists; # create a big zipfile

MakeAllUserPages $users, $watchlists; # create jsons and pdfs for each user

